﻿using NUnit.Framework;

using Yamabuki.Controller.Executer;
using Yamabuki.Design.Utility;
using Yamabuki.Design.Parser;
using Yamabuki.Utility.Xml;

namespace Yamabuki.Component.CastTest
{
    [TestFixture]
    public class DsStringToInteger_ExecuteDesign_Test
        : CastBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
        }

        /// <summary>
        /// DsRootSystem
        ///  DsString
        ///  DsStringToInteger
        ///  DsTerminator
        ///  
        /// DsString -> DsStringToInteger -> DsTerminator
        /// </summary>
        [Test]
        public void ExecuteDesign()
        {
            this.Collection.ClearAll();

            var r = this.CreateDsRootSystem("r");
            var s = this.CreateDsStringMock("s", r);
            var c = this.CreateDsStringToInteger("c", r);
            var t = this.CreateDsTerminatorMock("t", r);

            DsComponentConnectionUtils.ConnectForData(
                s.OutputPortCollection.GetPort(0),
                c.InputPortCollection.GetPort(0));
            DsComponentConnectionUtils.ConnectForData(
                c.OutputPortCollection.GetPort(0),
                t.InputPortCollection.GetPort(0));

            s.Value = "123";

            //  実行準備
            this.InitializeSequence(r);

            //  実行
            var executer = this.CreateSingleThreadExecuter(r);
            executer.Execute();

            Assert.AreEqual(t.Result, "123\r\n");
        }
    }
}
