﻿using NUnit.Framework;

using Yamabuki.Controller.Executer;
using Yamabuki.Design.Utility;
using Yamabuki.Design.Parser;
using Yamabuki.Utility.Xml;



namespace Yamabuki.Component.CastTest
{
    [TestFixture]
    public class DsIntegerToString_ExecuteDesign_Test
        : CastBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
        }

        /// <summary>
        /// DsRootSystem
        ///  DsInteger
        ///  DsIntegerToString
        ///  DsTerminator
        ///  
        /// DsInteger -> DsIntegerToString -> DsTerminator
        /// </summary>
        [Test]
        public void ExecuteDesign()
        {
            this.Collection.ClearAll();

            var r = this.CreateDsRootSystem("r");
            var i = this.CreateDsIntegerMock("i", r);
            var c = this.CreateDsIntegerToString("c", r);
            var t = this.CreateDsTerminatorMock("t", r);

            DsComponentConnectionUtils.ConnectForData(
                i.OutputPortCollection.GetPort(0),
                c.InputPortCollection.GetPort(0));
            DsComponentConnectionUtils.ConnectForData(
                c.OutputPortCollection.GetPort(0),
                t.InputPortCollection.GetPort(0));

            i.Value = 100;

            //  実行準備
            this.InitializeSequence(r);

            //  実行
            var executer = this.CreateSingleThreadExecuter(r);
            executer.Execute();

            Assert.AreEqual(t.Result, "100\r\n");
        }
    }
}
