﻿using NUnit.Framework;

using Yamabuki.Controller.Executer;
using Yamabuki.Design.Utility;
using Yamabuki.Design.Parser;
using Yamabuki.Utility.Xml;

namespace Yamabuki.Component.CastTest
{
    [TestFixture]
    public class DsStringToFloat_ExecuteDesign_Test
        : CastBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
        }

        /// <summary>
        /// DsRootSystem
        ///  DsString
        ///  DsStringToFloat
        ///  DsTerminator
        ///  
        /// DsString -> DsStringToFloat -> DsTerminator
        /// </summary>
        [Test]
        public void ExecuteDesign()
        {
            this.Collection.ClearAll();

            var r = this.CreateDsRootSystem("r");
            var s = this.CreateDsStringMock("s", r);
            var c = this.CreateDsStringToFloat("c", r);
            var t = this.CreateDsTerminatorMock("t", r);

            DsComponentConnectionUtils.ConnectForData(
                s.OutputPortCollection.GetPort(0),
                c.InputPortCollection.GetPort(0));
            DsComponentConnectionUtils.ConnectForData(
                c.OutputPortCollection.GetPort(0),
                t.InputPortCollection.GetPort(0));

            s.Value = "100.5";

            //  実行準備
            this.InitializeSequence(r);

            //  実行
            var executer = this.CreateSingleThreadExecuter(r);
            executer.Execute();

            Assert.AreEqual(t.Result, "100.5\r\n");
        }
    }
}
