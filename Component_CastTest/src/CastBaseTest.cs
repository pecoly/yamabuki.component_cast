﻿using System;
using System.IO;

using Yamabuki.Component.Cast;
using Yamabuki.Design.Component.Base;
using Yamabuki.Design.Utility;
using Yamabuki.Test;

namespace Yamabuki.Component.CastTest
{
    public class CastBaseTest
        : BaseTest
    {
        String nameSpace = "Yamabuki.Component.CastTest";

        public override void Initialize()
        {
            base.Initialize();

            String project = "Component_CastTest";
            String name = "yamabuki.component_cast";
            String target = "yamabuki";
            String bin = "bin\\Debug";
            Int32 pos;

            String path = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
            pos = path.IndexOf(project);
            if ( pos != -1 )
            {
                bin = path.Substring(pos + project.Length + 1);
            }

            pos = path.IndexOf(target, StringComparison.OrdinalIgnoreCase);
            if ( pos == -1 )
            {
                throw new DirectoryNotFoundException(path);
            }

            this.SolutionPath = path.Substring(0, pos + target.Length) + "\\" + name;
            this.ProjectPath = this.SolutionPath + "\\" + project;
            this.ExePath = Path.Combine(this.ProjectPath, bin);
        }

        public DsStringToInteger CreateDsStringToInteger(String name, DsComponentImpl parent)
        {
            return DsComponentCreationUtils.CreateComponent<DsStringToInteger>(this.Collection, 0, 0, name, parent);
        }

        public DsStringToFloat CreateDsStringToFloat(String name, DsComponentImpl parent)
        {
            return DsComponentCreationUtils.CreateComponent<DsStringToFloat>(this.Collection, 0, 0, name, parent);
        }

        public DsIntegerToString CreateDsIntegerToString(String name, DsComponentImpl parent)
        {
            return DsComponentCreationUtils.CreateComponent<DsIntegerToString>(this.Collection, 0, 0, name, parent);
        }

        public DsFloatToString CreateDsFloatToString(String name, DsComponentImpl parent)
        {
            return DsComponentCreationUtils.CreateComponent<DsFloatToString>(this.Collection, 0, 0, name, parent);
        }

        public override String ExePath { get; set; }
        public String SolutionPath { get; private set; }
        public String ProjectPath { get; private set; }
    }
}
