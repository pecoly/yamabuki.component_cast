﻿using NUnit.Framework;

using Yamabuki.Controller.Executer;
using Yamabuki.Design.Utility;
using Yamabuki.Design.Parser;
using Yamabuki.Utility.Xml;

namespace Yamabuki.Component.CastTest
{
    [TestFixture]
    public class DsFloatToString_ExecuteDesign_Test
        : CastBaseTest
    {
        [SetUp]
        public void SetUp()
        {
            this.Initialize();
        }

        /// <summary>
        /// DsRootSystem
        ///  DsFloat
        ///  DsFloatToString
        ///  DsTerminator
        ///  
        /// DsFloat -> DsFloatToString -> DsTerminator
        /// </summary>
        [Test]
        public void ExecuteDesign()
        {
            this.Collection.ClearAll();

            var r = this.CreateDsRootSystem("r");
            var f = this.CreateDsFloatMock("f", r);
            var c = this.CreateDsFloatToString("c", r);
            var t = this.CreateDsTerminatorMock("t", r);

            DsComponentConnectionUtils.ConnectForData(
                f.OutputPortCollection.GetPort(0),
                c.InputPortCollection.GetPort(0));
            DsComponentConnectionUtils.ConnectForData(
                c.OutputPortCollection.GetPort(0),
                t.InputPortCollection.GetPort(0));

            f.Value = 100f;

            //  実行準備
            this.InitializeSequence(r);

            //  実行
            var executer = this.CreateSingleThreadExecuter(r);
            executer.Execute();

            Assert.AreEqual(t.Result, "100\r\n");
        }
    }
}
