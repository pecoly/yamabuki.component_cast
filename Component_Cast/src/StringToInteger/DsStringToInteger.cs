﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;

using Yamabuki.Core.Message;
using Yamabuki.Design.Component.Simple;
using Yamabuki.Design.SimpleEditForm;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;
using Yamabuki.Task.Func;
using Yamabuki.Utility.Cast;

namespace Yamabuki.Component.Cast
{
    public class DsStringToInteger
        : DsSimpleComponent_1_1
    {
        public override String TypeName
        {
            get { return "StringToInteger"; }
        }

        public override int DefaultWidth
        {
            get { return 120; }
        }

        public override void Initialize(IEnumerable<XElement> data)
        {
            this.InitializeTypeImage("文字列 -> 整数");
        }

        public override BaseMessage DoubleClick()
        {
            var presenter = new DsSimpleEditForm_P(this);
            presenter.Show(
                FormSize.Auto,
                this.TypeName,
                "文字列を整数に変換します。");
            return null;            
        }

        protected override void Initialize_1_1()
        {
            this.InitializeTypeImage("文字列 -> 整数");
        }

        protected override TsTask GetTask(
            TsDataStoreGuid inputDataStoreGuid,
            TsDataStoreGuid outputDataStoreGuid)
        {
            var func = new Func<List<String>, List<Int32>>(list =>
            {
                var resultList = new List<Int32>(list.Count);
                var failureAction = new Func<String, Int32>((x) =>
                {
                    throw new InvalidCastException("文字列 -> 整数の変換に失敗しました。値 : " + x);
                });

                list.ForEach(x => resultList.Add(CastUtils.ToInt32(x, failureAction)));

                return resultList;
            });

            return new TsFuncTask_1_1__N_N<String, Int32>(
                this.DefinitionPath,
                inputDataStoreGuid,
                outputDataStoreGuid,
                func);
        }
    }
}
