﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;

using Yamabuki.Core.Message;
using Yamabuki.Design.Component.Simple;
using Yamabuki.Design.SimpleEditForm;
using Yamabuki.Task;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;
using Yamabuki.Task.Func;
using Yamabuki.Utility.Cast;

namespace Yamabuki.Component.Cast
{
    public class DsFloatToString
        : DsSimpleComponent_1_1
    {
        public override String TypeName
        {
            get { return "FloatToString"; }
        }

        public override int DefaultWidth
        {
            get { return 120; }
        }

        public override void Initialize(IEnumerable<XElement> data)
        {
            this.InitializeTypeImage("実数 -> 文字列");
        }

        public override BaseMessage DoubleClick()
        {
            var presenter = new DsSimpleEditForm_P(this);
            presenter.Show(
                FormSize.Auto,
                this.TypeName,
                "実数を文字列に変換します。");
            return null;            
        }

        protected override void Initialize_1_1()
        {
            this.InitializeTypeImage("実数 -> 文字列");
        }

        protected override TsTask GetTask(
            TsDataStoreGuid inputDataStoreGuid,
            TsDataStoreGuid outputDataStoreGuid)
        {
            var func = new Func<List<Single>, List<String>>(list =>
            {
                var resultList = new List<String>(list.Count);

                list.ForEach(x => resultList.Add(CastUtils.ToString(x)));

                return resultList;
            });

            return new TsFuncTask_1_1__N_N<Single, String>(
                this.DefinitionPath,
                inputDataStoreGuid,
                outputDataStoreGuid,
                func);
        }
    }
}
