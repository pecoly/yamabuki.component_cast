﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;

using Yamabuki.Core.Message;
using Yamabuki.Design.Component.Simple;
using Yamabuki.Design.SimpleEditForm;
using Yamabuki.Task.Base;
using Yamabuki.Task.Data;
using Yamabuki.Task.Func;
using Yamabuki.Utility.Cast;

namespace Yamabuki.Component.Cast
{
    public class DsStringToFloat
        : DsSimpleComponent_1_1
    {
        public override String TypeName
        {
            get { return "StringToFloat"; }
        }

        public override int DefaultWidth
        {
            get { return 120; }
        }

        public override void Initialize(IEnumerable<XElement> data)
        {
            this.InitializeTypeImage("文字列 -> 実数");
        }

        public override BaseMessage DoubleClick()
        {
            var presenter = new DsSimpleEditForm_P(this);
            presenter.Show(
                FormSize.Auto,
                this.TypeName,
                "文字列を実数に変換します。");
            return null;            
        }

        protected override void Initialize_1_1()
        {
            this.InitializeTypeImage("文字列 -> 実数");
        }

        protected override TsTask GetTask(
            TsDataStoreGuid inputDataStoreGuid,
            TsDataStoreGuid outputDataStoreGuid)
        {
            var func = new Func<List<String>, List<Single>>(list =>
            {
                var resultList = new List<Single>(list.Count);
                var failureAction = new Func<String, Single>((x) =>
                {
                    throw new InvalidCastException("文字列 -> 実数の変換に失敗しました。値 : " + x);
                });

                list.ForEach(x => resultList.Add(CastUtils.ToSingle(x, failureAction)));

                return resultList;
            });

            return new TsFuncTask_1_1__N_N<String, Single>(
                this.DefinitionPath,
                inputDataStoreGuid,
                outputDataStoreGuid,
                func);
        }
    }
}
